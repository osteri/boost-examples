# Boost example projects

Collection of Boost examples. Usually modified from stackoverflow answers/questions/examples.

## [socketcan](https://gitlab.com/osteri/boost-examples/blob/master/socketcan)
Project demonstrates on how to receive and filter CAN messages through the Linux socketcan interface. Uses Boost signals2 for binding callbacks and POSIX signals to handle interrupts.
## [strand](https://gitlab.com/osteri/boost-examples/blob/master/strand)
Project demostrates thread pools handling async tasks with synchronization (strands).
## [udp-coroutine](https://gitlab.com/osteri/boost-examples/blob/master/udp-coroutube)
Demonstrate receiving UDP packets with coroutines using single thread. Couldn't 
find example of this 
anywhere. TCP examples are all over the internet but UDP examples are not.