# Boost ASIO strands

Using C++11 standard library thread pools and offload tasks with async handlers.

Starts n number of threads and prints counter from 1-5 asynchronicly with and without using strands.
 
## Build
```
cmake -S . -B build && cd build && make -j
```
