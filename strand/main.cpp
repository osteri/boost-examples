#define BOOST_DATE_TIME_NO_LIB
#define BOOST_REGEX_NO_LIB

#include <boost/asio.hpp>
#include <boost/bind/bind.hpp>

#include <chrono>
#include <iostream>
#include <mutex>
#include <thread>

static constexpr unsigned short num_threads = 5;

std::mutex print_mutex;

void WorkerThread(boost::asio::io_service& io_svc, int counter) {
  {
    std::lock_guard lg{print_mutex};
    std::cout << "Thread " << counter << " start\n";
  }

  io_svc.run();

  std::lock_guard lg{print_mutex};
  std::cout << "Thread " << counter << " end\n";
}

void async_send_handler(int number) {
  std::lock_guard lg{print_mutex};
  std::cout << "Number: " << number << '\n';
}

int main(void) {
  boost::asio::io_service io_svc;
  boost::asio::io_service::strand strand(io_svc);
  boost::asio::io_service::work worker(io_svc);
  std::cout << "The program will exit once all work has finished.\n";

  // Worker pool
  std::vector<std::thread> threads(num_threads);

  // Start threads
  std::for_each(threads.begin(), threads.end(), [&io_svc](std::thread& thd) {
    static std::size_t cnt{1};
    thd = std::thread(std::bind(&WorkerThread, std::ref(io_svc), cnt++));
  });

  // Wait threads to start
  std::this_thread::sleep_for(std::chrono::milliseconds(500));

  // This code is almost equal to calling handlers of socket.async_send.
  // The handlers are invoked concurently and the order might be arbitrary
  io_svc.post(boost::bind(&async_send_handler, 1));
  io_svc.post(boost::bind(&async_send_handler, 2));
  io_svc.post(boost::bind(&async_send_handler, 3));
  io_svc.post(boost::bind(&async_send_handler, 4));
  io_svc.post(boost::bind(&async_send_handler, 5));

  // This code will do what you exactly want;
  // It will execute the handlers sequentially in that order
  strand.post(boost::bind(&async_send_handler, 1));
  strand.post(boost::bind(&async_send_handler, 2));
  strand.post(boost::bind(&async_send_handler, 3));
  strand.post(boost::bind(&async_send_handler, 4));
  strand.post(boost::bind(&async_send_handler, 5));

  worker.~work();

  // Join all threads
  std::for_each(threads.begin(), threads.end(),
                [&io_svc](std::thread& thd) { thd.join(); });

  return EXIT_SUCCESS;
}
