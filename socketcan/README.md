# SocketCAN via Boost ASIO

Calculate SocketCan filter on Matlab/Octave:
```
bitand(incoming_canid, mask) == bitand(canid, mask)
```

## Build
```
cmake -S . -B build && cd build && make -j
```
