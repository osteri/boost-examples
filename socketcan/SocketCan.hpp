#include <linux/can.h>
#include <linux/can/raw.h>
#include <net/if.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/ioctl.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>

#include <boost/asio.hpp>
#include <boost/bind/bind.hpp>
#include <boost/signals2.hpp>
#include <initializer_list>
#include <iostream>
#include <map>

struct DriverData {
  sockaddr_can addr;
  ifreq ifr;
  int s;
};

class SocketCan {
  using rx_buffer_t = can_frame;
  using tx_buffer_t = can_frame;

  using rx_handler = std::function<void(
      rx_buffer_t& buf, boost::asio::posix::basic_stream_descriptor<>& stream)>;

  using rx_signal_t = boost::signals2::signal<void(const can_frame& frame)>;

 private:
  boost::asio::io_service& m_io;
  rx_buffer_t m_rx_buffer;
  tx_buffer_t m_tx_buffer;

  boost::asio::posix::basic_stream_descriptor<> m_stream;
  std::vector<const char*> m_interfaces;
  std::map<const char*, DriverData>
      m_data;  // key is an interface, for example: "vcan0", "can0", etc..

  std::size_t m_total_frames_received;

 public:
  SocketCan(boost::asio::io_service& io,
            const std::initializer_list<const char*> interfaces,
            const can_filter* filters);
  ~SocketCan();

  void print_interfaces() const;

  // Call rx signals
  rx_handler rx_cb;

  // Signals user can subscribe to
  rx_signal_t on_rx_error;  // TODO:
  rx_signal_t on_rx;

  // Filter for incoming CAN ids. Linux bitwise masking can't handle all
  // possible corner cases.
  std::function<bool(const can_frame& f)> guard_fn;
};

SocketCan::SocketCan(boost::asio::io_service& io,
                     const std::initializer_list<const char*> interfaces,
                     const can_filter* filters)
    : m_io{io}, m_stream{io}, m_interfaces{interfaces} {
  // Listen all interfaces by default if no explicit iface given
  if (m_interfaces.size() == 0) m_interfaces.emplace_back("any");

  for (const char* iface : m_interfaces) {
    DriverData data;

    data.s = socket(PF_CAN, SOCK_RAW, CAN_RAW);
    std::strncpy(data.ifr.ifr_name, iface, sizeof(&iface));
    ioctl(data.s, SIOCGIFINDEX, &(data.ifr));
    data.addr.can_family = AF_CAN;
    data.addr.can_ifindex =
        strcmp("any", iface) == 0 ? 0 : data.ifr.ifr_ifindex;
    if (bind(data.s, (struct sockaddr*)&(data.addr), sizeof(data.addr)) < 0) {
      throw std::runtime_error("unable to bind socket");
    }

    setsockopt(data.s, SOL_CAN_RAW, CAN_RAW_FILTER, filters, sizeof(*filters));

    m_stream.assign(data.s);
    m_data[iface] = data;
  }

  // Re-attach readers to boost asio and call signals
  rx_cb = [this](rx_buffer_t& in,
                 boost::asio::posix::basic_stream_descriptor<>& stream) {
    if (guard_fn && guard_fn(in)) {
      on_rx(in);
    } else if (!guard_fn) {
      on_rx(in);
    }

    m_stream.async_read_some(
        boost::asio::buffer(&m_rx_buffer, sizeof(m_rx_buffer)),
        boost::bind(rx_cb, std::ref(m_rx_buffer), std::ref(m_stream)));
  };

  m_stream.async_read_some(
      boost::asio::buffer(&m_rx_buffer, sizeof(m_rx_buffer)),
      boost::bind(rx_cb, std::ref(m_rx_buffer), std::ref(m_stream)));
}

SocketCan::~SocketCan() {
  for (const auto& d : m_data) {
    std::cout << "closing interfaces: " << d.first << ' ';
    close(d.second.s);
  };
  std::cout << std::endl;
}

void SocketCan::print_interfaces() const {
  for (const auto& d : m_data) {
    std::cout << "binded to following interfaces: " << d.first << ' ';
  }
  std::cout << std::endl;
}