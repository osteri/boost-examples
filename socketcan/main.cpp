#include <boost/asio.hpp>
#include <boost/asio/signal_set.hpp>
#include <boost/bind/bind.hpp>
#include <iostream>

#include "SocketCan.hpp"

template <const std::size_t N>
constexpr bool div_by(const can_frame& frame) {
  return (frame.can_id % N) == 0;
}

template <class T>
std::string num_to_hex(T n) {
  std::ostringstream ss;
  ss << std::uppercase << std::hex << "0x" << n;
  return ss.str();
}

// When a POSIX signal occurs, process them here
void sig_handler(boost::asio::signal_set& signals,
                 boost::system::error_code error, int signal_number) {
  if (!error) {
    static std::atomic<std::size_t> cnt;
    cnt++;
    if (signal_number == SIGALRM) {
      std::cout << "A " << cnt << " SIGALRM occurred, re-starting...\n";
      signals.async_wait(boost::bind(sig_handler, boost::ref(signals),
                                     boost::placeholders::_1,
                                     boost::placeholders::_2));
      alarm(1);
    } else if (signal_number == SIGINT) {
      throw std::runtime_error("SIGINT called");
    }
  } else {
    std::cerr << "received boost error code: " << error << '\n';
  }
}

int main() {
  boost::asio::io_service io;

  // Print every second, unless blocked by CAN frame processing
  boost::asio::signal_set signals(io, SIGINT, SIGALRM);
  signals.async_wait(boost::bind(sig_handler, boost::ref(signals),
                                 boost::placeholders::_1,
                                 boost::placeholders::_2));
  alarm(1);

  // Print frame IDs, block POSIX signals during processing
  auto frame_print = [&signals](const can_frame& frame) {
    signals.clear();
    std::cout << "received frame: " << num_to_hex(frame.can_id) << '\n';
    signals.add(SIGALRM);
    signals.add(SIGINT);
  };

  // Open all available interfaces without filters and print everything
  // Simplest use case possible
  SocketCan can_all{io, {}, {}};
  can_all.print_interfaces();
  can_all.on_rx.connect(frame_print);  // subscribe to CAN RX events

  // Open "can0" and receive only 0x42x CAN ids
  const can_filter filters[] = {{.can_id = 0x420, .can_mask = 0xFF0}};
  SocketCan can0{io, {"can0"}, filters};
  can0.print_interfaces();
  can0.guard_fn = div_by<8>;        // frame id must be dividable by 8
  can0.on_rx.connect(frame_print);  // subscribe to CAN RX events

  try {
    io.run();
  } catch (const std::exception& e) {
    std::cerr << e.what() << '\n';
  }

  return EXIT_SUCCESS;
}
