# Boost ASIO UDP receiver with coroutines
 
Uses port `1895` for receiving.

## Build
```sh
cmake -S . -B build && cd build && make -j
```
## Test sending data to UDP receiver
Exit server by pressing Ctrl+C
1. terminal (UDP server)
```sh
./build/udp-coroutine
```
2. terminal (send data)
```sh
echo "Hello!" >/dev/udp/127.0.0.1/1895
```
Terminal 1. output:
```
received: 'Hello!', 7 bytes
```