#include <boost/asio/detached.hpp>
#include <boost/asio/io_context.hpp>
#include <boost/asio/ip/udp.hpp>
#include <boost/asio/signal_set.hpp>
#include <boost/asio/use_awaitable.hpp>
#include <boost/asio/co_spawn.hpp>
#include <iostream>

using boost::asio::awaitable;
using boost::asio::co_spawn;
using boost::asio::detached;
using boost::asio::use_awaitable;
using boost::asio::ip::udp;
namespace this_coro = boost::asio::this_coro;

#if defined(BOOST_ASIO_ENABLE_HANDLER_TRACKING)
#define use_awaitable \
  boost::asio::use_awaitable_t(__FILE__, __LINE__, __PRETTY_FUNCTION__)
#endif

awaitable<void> echo(udp::socket socket) {
  try {
    boost::asio::ip::udp::endpoint ep;
    char data[1024];
    for (;;) {
      std::size_t n = co_await socket.async_receive_from(
          boost::asio::buffer(data), ep, use_awaitable);
      std::cout << "received: \'" << std::string{data, n - 1}
                << "\', " << n << " bytes \n";
    }
  } catch (std::exception& e) {
    std::cerr << "echo exception: " << e.what() << '\n';
  }
}

awaitable<void> listener() {
  auto executor = co_await this_coro::executor;
#pragma clang diagnostic push
#pragma ide diagnostic ignored "EndlessLoop"
  for (;;) {
    co_spawn(executor,
             echo(udp::socket{
                 udp::socket{executor, boost::asio::ip::udp::endpoint(
                                           boost::asio::ip::udp::v4(), 1895)}}),
             detached);
  }
#pragma clang diagnostic pop
}

int main() {
  boost::asio::io_context io{1};

  auto signals = std::make_shared<boost::asio::signal_set>(io, SIGINT, SIGTERM);
  signals->async_wait([&io, signals](const boost::system::error_code& ec,
                                     int sig_num) -> void { io.stop(); });
  co_spawn(io, listener(), detached);
  io.run();
}